module.exports = function (sequelize, dataTypes) {
  console.log('Defining model ProductionAreaVersion');
  var ProductionAreaVersion = sequelize.define('production_area_version', {
    current: {
      type: dataTypes.BOOLEAN,
      allowNull: false,
    }
  }, {
    tableName: 'production_area_version',
    freezeTableName: true,
    timestamps: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        console.log('Building associations of ProductionArea');
        ProductionAreaVersion.hasMany(models.ProductionArea);
      }
    }
  });

  return ProductionAreaVersion;
};

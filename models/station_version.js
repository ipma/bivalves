module.exports = function (sequelize, dataTypes) {
  console.log('Defining model StationVersion');
  var StationVersion = sequelize.define('station_version', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true
    },
    current: {
      type: dataTypes.BOOLEAN,
      allowNull: false,
    }
  }, {
    tableName: 'station_version',
    freezeTableName: true,
    timestamps: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        console.log('Building associations of Station');
        StationVersion.hasMany(models.Station);
      }
    }
  });

  return StationVersion;
};

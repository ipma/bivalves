module.exports = function(sequelize, dataTypes) {
  console.log('Defining model Station');
  var Station = sequelize.define('station', {
    id: {
      type: dataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: dataTypes.STRING(128),
      allowNull: false
    },
    lat: {
      type: dataTypes.FLOAT,
      allowNull: false,
      unique: 'unique_station_lat_lon'
    },
    lon: {
      type: dataTypes.FLOAT,
      allowNull: false,
      unique: 'unique_station_lat_lon'
    },
    active: {
      type: dataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  }, {
    tableName: 'station',
    freezeTableName: true,
    timestamps: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        console.log('Building associations of Station');
        Station.belongsTo(models.ProductionArea);
        Station.hasMany(models.Sample);
      }
    }
  });

  return Station;
};

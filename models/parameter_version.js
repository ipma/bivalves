module.exports = function (sequelize, dataTypes) {
  console.log('Defining model ParameterVersion');
  var ParameterVersion = sequelize.define('parameter_version', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true
    },
    current: {
      type: dataTypes.BOOLEAN,
      allowNull: false,
    }
  }, {
    tableName: 'parameter_version',
    freezeTableName: true,
    timestamps: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        console.log('Building associations of Parameter');
        ParameterVersion.hasMany(models.Parameter);
      }
    }
  });

  return ParameterVersion;
};

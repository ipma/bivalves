module.exports = function(sequelize, dataTypes) {
  console.log('Defining model Parameter');
  var Parameter = sequelize.define('parameter', {
    id: {
      type: dataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: dataTypes.STRING(128),
      allowNull: false
    },
    group: {
      type: dataTypes.STRING(128),
      allowNull: true
    }
  }, {
    tableName: 'parameter',
    freezeTableName: true,
    timestamps: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        console.log('Building associations of Parameter');
        Parameter.hasMany(models.SampleParameter);
      }
    }
  });

  return Parameter;
};

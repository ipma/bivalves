/* -------------------------------------------------------------------
 * Models
 *
 * This file is the only require that is needed to work with the
 * application models.
 *
 * It automatically, on first import, generates the database tables
 * and builds the sequelize objects.
 * ---------------------------------------------------------------- */

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var database  = require('../config').database;


/* Common objects: database connection and models aggregator */
var sequelize = new Sequelize(database.url);
var models    = {};


/* Registry of the models to be loaded, identified by the file where they
 * are defined, and indicating the name we want these objects to have in
 * the global namespace
 */
var modelList = [
  {
    file: 'production_area.js',
    model: 'ProductionArea'
  },{
    file: 'station.js',
    model: 'Station'
  },{
    file: 'sample.js',
    model: 'Sample'
  },{
    file: 'parameter.js',
    model: 'Parameter',
  },{
    file: 'sample_parameter.js',
    model: 'SampleParameter'
  },{
    file: 'production_area_version.js',
    model: 'ProductionAreaVersion'
  },{
    file: 'station_version.js',
    model: 'StationVersion'
  },{
    file: 'parameter_version.js',
    model: 'ParameterVersion'
  }
];


/* Model dynamic loading. First we create all the model objects and only
 * after we call the methods that create the relationships between them.
 * This way, we avoid not defined like-errors
 */
modelList.forEach(function(element) {
  var newModel = sequelize.import(path.join(__dirname, element.file));
  models[element.model] = newModel;
});

console.log('Loaded ', models);

Object.keys(models).forEach(function(element) {
  if ('associate' in models[element]) {
    models[element].associate(models);
  }
});


/* Final setup of the model object. We also pass outside the sequelize
 * database connection object (required to sync the database at startup)
 * and the sequelize module itself.
 */

models.sequelize = sequelize;
models.Sequelize = Sequelize;

/* Final export of models and database connection */
module.exports = models;

module.exports = function(sequelize, dataTypes) {
  console.log('Defining model SampleParameter');
  var SampleParameter = sequelize.define('sample_parameter', {
    id: {
      type: dataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    value: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
  }, {
    tableName: 'sample_parameter',
    freezeTableName: true,
    timestamps: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        console.log('Building associations of SampleParameter');
        SampleParameter.belongsTo(models.Sample);
        SampleParameter.belongsTo(models.Parameter);
      }
    }
  });

  return SampleParameter;
};


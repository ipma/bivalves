var models = require('../models');


module.exports = {
  get_current_production_areas: function () {
    /*return models.sequelize.query(
      'SELECT pa.code, pa.name, pa.type ' +
      'FROM production_area AS pa, production_area_version AS pav ' +
      'WHERE pa.production_area_version_id=pav.id AND pav.current=true'
    );*/

    return models.ProductionArea.findAll({
      include: [{
        model: models.ProductionAreaVersion,
        where: {
          current: true
        }
      }]
    });
  },

  get_production_areas_by_version: function (version) {
    /*return models.sequelize.query(
      'SELECT pa.code, pa.name, pa.class ' +
      'FROM production_area AS pa ' +
      'WHERE pa.production_area_version_id=' + version
    );*/
    return models.ProductionArea.findAll({
      where: {
        production_area_version_id: version
      }
    });
  },

  get_current_stations: function () {
    return models.sequelize.query(
      'SELECT s.id, s.name, s.lat, s.lon, s.active ' +
      'FROM station AS s, station_version AS sv ' +
      'WHERE station.station_version_id=sv.id AND sv.current=true'
    );
  },

  get_stations_by_version: function (version) {
    return models.sequelize.query(
      'SELECT s.id, s.name, s.lat, s.lon, s.active ' +
      'FROM station AS s ' +
      'WHERE s.station_version_id=' + version
    );
  },

  get_current_parameters: function () {
    return models.sequelize.query(
      'SELECT p.id, p.name, p.group ' +
      'FROM parameter AS p, parameter_version AS pv ' +
      'WHERE parameter.parameter_version_id=pv.id AND pv.current=true'
    );
  },

  get_parameters_by_version: function (version) {
    return models.sequelize.query(
      'SELECT p.id, p.name, p.group ' +
      'FROM parameter AS p ' +
      'WHERE p.parameter_version_id=' + version
    );
  },
};

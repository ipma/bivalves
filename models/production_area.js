module.exports = function(sequelize, dataTypes) {
  console.log('Defining model ProductionArea');
  var ProductionArea = sequelize.define('production_area', {
    code: {
      type: dataTypes.STRING(32),
      allowNull: false,
    },
    name: {
      type: dataTypes.STRING(128),
      allowNull: false,
    },
    type: {
      type: dataTypes.ENUM(['litoral', 'estuarino-lagunar']),
      allowNull: false
    },
    class: {
      type: dataTypes.STRING(4),
      allowNull: true
    }
  }, {
    tableName: 'production_area',
    freezeTableName: true,
    timestamps: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        console.log('Building associations of production_area');
        ProductionArea.hasMany(models.Station);
        ProductionArea.belongsTo(models.ProductionAreaVersion);
      }
    }
  });

  return ProductionArea;
};

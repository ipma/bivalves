module.exports = function(sequelize, dataTypes) {
  console.log('Defining model Sample');
  var Sample = sequelize.define('sample', {
    id: {
      type: dataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    year: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    year_count: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    sampling_datetime: {
      type: dataTypes.DATE,
      allowNull: true
    },
    reception_datetime: {
      type: dataTypes.DATE,
      allowNull: true
    },
    observation_datetime: {
      type: dataTypes.DATE,
      allowNull: true
    },
    temperature: {
      type: dataTypes.FLOAT,
      allowNull: true
    },
    salinity: {
      type: dataTypes.FLOAT,
      allowNull: true
    },
    sample_volume: {
      type: dataTypes.FLOAT,
      allowNull: true
    },
    observer: {
      type: dataTypes.STRING(128),
      allowNull: true
    },
    notes: {
      type: dataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'sample',
    freezeTableName: true,
    timestamps: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        console.log('Building associations of Sample');
        Sample.belongsTo(models.Station);
      },
    },
    instanceMethods: {
      sumToxicity: function(group) {
        console.log(this);
        var query = "SELECT sum(sp.value) FROM sample_parameter as sp, parameter as p WHERE sp.parameter_id=p.id AND p.group='" + group + "' AND sp.sample_id=" + this.id + " GROUP BY sp.sample_id";
        return sequelize.query(query).then(function(results,metadata) {
          console.log(results);
          console.log(metadata);
        });
      }
    }
  });

  return Sample;
};

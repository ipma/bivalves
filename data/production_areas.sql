/* Zonas de Produção */
BEGIN;
/*INSERT INTO production_area_version VALUES (1, true, NOW(), NOW());*/
INSERT INTO production_area VALUES (DEFAULT, 'L1', 'Litoral Viana', 'litoral', 'A', now(), now(), 1);
INSERT INTO production_area VALUES (DEFAULT, 'L2', 'Litoral Matosinhos', 'litoral', 'A', now(), now(), 1);
INSERT INTO production_area VALUES (DEFAULT, 'L3', 'Litoral Aveiro', 'litoral', 'A', now(), now(), 1);
INSERT INTO production_area VALUES (DEFAULT, 'L4', 'Litoral Figueira da Foz - Nazaré', 'litoral', 'A', now(), now(), 1);
INSERT INTO production_area VALUES (DEFAULT, 'L5', 'Litoral Peniche - Lisboa', 'litoral', 'A', now(), now(), 1);
INSERT INTO production_area VALUES (DEFAULT, 'L6', 'Litoral Setúbal - Sines', 'litoral', 'A', now(), now(), 1);
INSERT INTO production_area VALUES (DEFAULT, 'L7a', 'Litoral Aljezur - S. Vicente', 'litoral', 'A', now(), now(), 1);
INSERT INTO production_area VALUES (DEFAULT, 'L7b', 'Litoral Offshore', 'litoral', 'A', now(), now(), 1);
INSERT INTO production_area VALUES (DEFAULT, 'L7c', 'Litoral S. Vicente - Portimão', 'litoral', 'A', now(), now(), 1);
INSERT INTO production_area VALUES (DEFAULT, 'L8', 'Litoral Faro - Olhão', 'litoral', 'A', now(), now(), 1);
INSERT INTO production_area VALUES (DEFAULT, 'L9', 'Litoral Tavira - V. R. S. António', 'litoral', 'A', now(), now(), 1);
COMMIT;

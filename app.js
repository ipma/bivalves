var express         = require('express');
var path            = require('path');
var logger          = require('morgan');
var log4js          = require('log4js');
var session         = require('express-session');
var favicon         = require('serve-favicon');
var cookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var pg              = require('pg');
var pgSession       = require('connect-pg-simple')(session);

/* Application specific requires. */
var auth            = require('./auth');
var routes          = require('./routes/index');
var users           = require('./routes/users');
var api_controllers = require('./controllers/api');
var dashboard       = require('./controllers/dashboard');

/* Application Setup */
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

/* ---------------------------------------------------------------
 * Production logging configuration
 * ------------------------------------------------------------ */
if (app.get('env') !== 'development') {
  log4js.configure({
    appenders: [
      { type: 'console' , layout: 'basic'},
      //{ type: 'file', filename: '/var/log/bivalves.log', category: 'bivalves' }
    ]
  });
  var appLogger = log4js.getLogger();
  app.use(logger({
    'format': 'default',
    'stream': {
      write: function(str) {
        appLogger.debug(str);
      }
    }
  }));
} else {
  app.use(logger('dev'));
}


/* ---------------------------------------------------------------
 * Application settings:
 * - cookie settings
 * - static files settings
 * - session settings
 * ------------------------------------------------------------ */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  store: new pgSession({
    pg: pg,
    conString: 'postgres://pedro:pedro@localhost/bivalves',
    tableName: 'user_session'
  }),
  secret: 'super duper secret',
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 24 * 60 * 60 * 1000 }
}));


/* ---------------------------------------------------------------
 * Routes setup
 * ------------------------------------------------------------ */
app.use('/', routes); // Switch to management
//app.use('/users', users);

/* Switching to the new model/controller/view scheme */
app.use('/api', api_controllers);
app.use('/dashboard', auth.restrict, dashboard);


/* ---------------------------------------------------------------
 * Error handling and other Express default stuff
 * ------------------------------------------------------------ */
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;

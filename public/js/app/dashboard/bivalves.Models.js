var ProductionArea  = Backbone.Model.extend({});
var ProductionAreas = Backbone.Collection.extend({
  model: ProductionArea,
  url: '/api/production_area'
});
//var production_areas_collection = new ProductionAreas();


var Station  = Backbone.Model.extend({
  isActive: function() {
    if (this.get('active') === true) {
      return 'Activa';
    } else {
      return 'Inactiva';
    }
  }
});
var Stations = Backbone.Collection.extend({
  model: Station,
  url: '/api/station'
});
var stations_collection = new Stations();

var Parameter = Backbone.Model.extend({});
var Parameters = Backbone.Collection.extend({
  model: Parameter,
  url: '/api/parameter'
});
var parameters_collection = new Parameters();


var SampleParameter = Backbone.Model.extend({});
var SampleParameters = Backbone.Collection.extend({
  model: SampleParameter,
  url: '/api/sample_parameter'
});
var sample_parameters_collection = new SampleParameters();


var Sample = Backbone.Model.extend({
  stationName: function() {
    var stationModel = stations_collection.get(this.get('station_id'));
    return stationModel.get('name');
  },

  stationProductionArea: function () {
    var stationModel = stations_collection.get(this.get('station_id'));
    return stationModel.get('production_area_code');
  },

  sampleGroupSums: function () {
    var buckets = {
      'asp': 0,
      'dsp': 0,
      'psp': 0
    };
    var parameters = sample_parameters_collection.where({
      sample_id: this.get('id')
    });
    for (var i = 0; i < parameters.length; i++) {
      var parameter_group = parameters_collection.get(parameters[i].get('parameter_id')).get('group');
      if (buckets.hasOwnProperty(parameter_group)) {
        buckets[parameter_group] += parameters[i].get('value');
      }
    }
    return buckets;
  },

  sampleASP: function () {
    return this.sampleGroupSums().asp;
  },

  sampleDSP: function () {
    return this.sampleGroupSums().dsp;
  },

  samplePSP: function () {
    return this.sampleGroupSums().psp;
  }
});
var Samples = Backbone.Collection.extend({
  model: Sample,
  url: '/api/sample'
});
var samples_collection = new Samples();

var cols = [{
  name: 'id',
  label: 'ID',
  editable: false,
  cell: 'integer'
}, {
  name: 'name',
  label: 'Nome',
  editable: false,
  cell: 'string'
}, {
  name: 'lat',
  label: 'Latitude',
  editable: false,
  cell: 'number'
}, {
  name: 'lon',
  label: 'Longitude',
  editable: false,
  cell: 'number'
}, {
  name: 'production_area_code',
  label: 'Zona de produção',
  editable: false,
  cell: 'string'
}, {
  name: 'active',
  label: 'Activa',
  editable: false,
  cell: 'string'
}];

var grid = new Backgrid.Grid({
  columns: cols,
  collection: stations_collection
});

$('#main_table').append(grid.render().el);
stations_collection.fetch({reset: true});

function Validation () {
  this.fields = {};

  this.register_field = function (field, msg) {
    this.fields[field] = {
      msg: msg,
      state: false
    };
  };

  this.toggle_field = function (field) {
    this.fields[field].state = !this.fields[field].state;
  };

  this.set_field_state = function (field, state) {
    this.fields[field].state = state;
    this.set_messages();
  };

  this.set_messages = function () {
    for (var field in this.fields) {
      if (this.fields[field].state && $('#alert-container div[data-field=' + field + ']').length < 1) {
        $('#alert-container').append('<div class="alert alert-danger" role="alert" data-field="' + field + '" style="display: none">' + this.fields[field].msg + '</div>');
        $('#alert-container div[data-field=' + field + ']').slideDown();
      } else if (!this.fields[field].state) {
        $('#alert-container div[data-field=' + field + ']').slideUp('slow', function () {
          $(this).remove();
        });
      }
    }

    this.hide_upload_error();
  };

  this.hide_upload_error = function () {
    var state = _.keys(this.fields).every(function(curr, index, arr) {
      return this.fields[curr].state == false;
    }, this);

    console.log(state);
    if (state && $('#alert-container').has('div[data-upload=false]')) {
      $('#alert-container div[data-upload=false]').remove();
    }
  };
}

var validation = new Validation();
validation.register_field('date', 'Verifique se as datas de colheita, recepção e observação estão correctas.');
validation.register_field('count', 'Verifique o número da amostra. Actual número da amostra inválido ou já atribuido.');

function generateParameterCombobox() {
  var options = $('#parameter_table option:selected').map(function(index, elm) {
    return elm.value;
  });

  var combobox = '<select class="form-control">'
  parameters_collection.forEach(function(element) {
    var option = '<option value="' + element.get('id') + '">';
    option += element.get('name');
    option += '</option>';
    combobox += option;
  });

  return combobox + '</select>';
}


function updateProductionArea() {
  var val = parseInt($('#station').val());
  var station_prod_area = stations_collection.get(val).get('production_area_code');
  var text = production_areas_collection.findWhere({code: station_prod_area}).get('name');
  $('#production_area_code').text(station_prod_area);
  $('#production_area').text(text);
}


function buildFormData() {
  var valid = true;
  var mandatory_missing = false;
  var form_data = new Object();
  [
    'sampling_datetime',
    'reception_datetime',
    'observation_datetime',
    'temperature',
    'salinity',
    'sample_volume',
    'station',
    'year_count',
  ].forEach(function(element) {
    form_data[element] = $('#' + element).val();
    if (element === 'salinity' || element === 'temperature') {
      if (form_data[element].length < 1) {
        form_data[element] = null;
      }
    }

    if (element.indexOf('datetime') > -1) {

      if (element.indexOf('observation') < 0) {
        var ts = new Date($('#' + element).val().replace(' ', 'T') + 'Z');
      } else {
        var ts = new Date($('#' + element).val().replace(' ', 'T'));
      }

      if (element.indexOf('observation') > -1) {
        form_data[element] = moment.utc(ts).format('YYYY-MM-DD');
      } else {
        form_data[element] = moment.utc(ts).format('YYYY-MM-DD HH:mm');
      }
    }

    if (element.indexOf('datetime') > -1 && form_data[element].length < 1) {
      mandatory_missing = true;
      valid = false;
    }
  });

  if (mandatory_missing) {
    $('#alert-container').append('<div class="alert alert-danger" role="alert" data-upload="false">Existem erros no formulário que precisam de ser corrigidos para poder guardar os dados.</div>');
  }

  if ($('#alert-container').has('.alert[data-upload!=false]').length && valid) {
    valid = false;
    if ($('#alert-container').has('.alert[data-upload=false]').length < 1) {
      $('#alert-container').append('<div class="alert alert-danger" role="alert" data-upload="false">Existem erros no formulário que precisam de ser corrigidos para poder guardar os dados.</div>');
    }
  }

  var observer = $('#observer').val();
  if (observer === 'Outro') {
    observer = $('#observer_other').val();
  }
  form_data['observer'] = observer;

  var notes = $('#notes').val();
  if (notes === 'Outro') {
    notes = $('#notes_other').val();
  }
  form_data['notes'] = notes;

  form_data['parameters'] = [];
  $('#parameter_table select').each(function(index, elm) {
    var new_param = new Object();
    new_param['parameter'] = parseInt($(elm).val());
    new_param['value'] = parseInt($('#parameter_table input')[index].value);
    form_data.parameters.push(new_param);
  });

  if(valid) {
    if ($('#alert-container').has('.alert')) {
      $('#alert-container').empty();
    }
    return form_data;
  } else {
    return null;
  }
}


function submitFormData(data) {
  $.ajax({
    type: 'POST',
    url: '/dashboard/samples/new',
    data: JSON.stringify(data),
    dataType: 'json',
    contentType: "application/json",
    success: function (data) {
      location.pathname= data.redirect;
    }
  });
}


function doUpload() {
  var form_data = buildFormData();
  if (form_data !== null) {
    submitFormData(form_data);
  }
}


function recalculateSums() {
  var sums = {
    asp: 0,
    dsp: 0,
    psp: 0
  };

  var rows = $('#parameter_table tbody tr');
  rows.each(function(index, elm) {
    var param_id = $(elm).find("select").val();
    var value = $(elm).find("input").val();

    if (!param_id) {
      sums.asp = sums.dsp = sums.psp = 0;
    } else {
      var group = parameters_collection.get(parseInt(param_id)).get('group');
      if (group && sums.hasOwnProperty(group)) {
        sums[group] += parseInt(value);
      }
    }
  });

  for (var key in sums) {
    $('#' + key).val(sums[key]);
  }
}


function validateDateTime(dt) {
  /* First validate just the string format. The datetime string must have a format
   * of the type YYYY-MM-DD HH:MM, where the date separators can be -/\ and the time
   * separator must be :
   */
  var sampling = new Date($('#sampling_datetime').val().replace(' ', 'T'));
  var reception = new Date($('#reception_datetime').val().replace(' ', 'T'));
  var observation = new Date($('#observation_datetime').val());
  if (sampling > reception || reception > observation || sampling > observation) {
    $('#sampling_datetime').parent().addClass('has-error');
    $('#reception_datetime').parent().addClass('has-error');
    $('#observation_datetime').parent().addClass('has-error');
    validation.set_field_state('date', true);
  } else {
    $('#sampling_datetime').parent().removeClass('has-error');
    $('#reception_datetime').parent().removeClass('has-error');
    $('#observation_datetime').parent().removeClass('has-error');
    validation.set_field_state('date', false);
  }
}


function validateFloat(n) {
  var f = parseFloat(n);
  return !isNaN(f);
}

function validateYearCount() {
  var curr = parseInt($('#year_count').val());
  if (curr == null || curr == undefined || isNaN(curr)) {
    return false;
  } else if (curr <= 0) {
    return false;
  }

  var year_counts_list = _.map(samples_collection.models, function(sample) {
    return sample.attributes.year_count;
  });

  if (_.indexOf(year_counts_list, curr) > -1) {
    return false;
  }

  return true;
};


function parseParameterTable() {
  var table_rows = $('#parameter_table tbody tr');
  var rows = [];
  $.each(table_rows, function(i, value) {
    var row = {
      index: i,
      value: value.firstChild.firstChild.value,
      element: value.firstChild.firstChild
    };
    rows.push(row);
  });

  return rows;
}



$(document).ready(function() {
  $(document).keydown(function(e) { if (e.keyCode == 8) $(document).focus(); });

  var table = $("#parameter_table").DataTable({
    paging: false,
    searching: false,
    info: false,
    autoWidth: false,
    sorting: false,
    language: {
      emptyTable: 'Sem dados para esta tabela'
    }
  });

  /* Validation */
  $('#sample-form').validator();

  /* Date picker */
  $('#sampling_datetime').datetimepicker({
    format: 'YYYY-MM-DD HH:mm'
  });
  $('#reception_datetime').datetimepicker({
    format: 'YYYY-MM-DD HH:mm'
  });
  $('#observation_datetime').datetimepicker({
    format: 'YYYY-MM-DD'
  });

  /* We bind all Backbone events here, before calling any fetching that will sync
   * the collections with the database.
   */
  var fetch_list = _.invoke([
      stations_collection,
      samples_collection,
      parameters_collection,
      production_areas_collection,
      sample_parameters_collection
  ], 'fetch');

  $.when.apply($, fetch_list).done(function() {
    updateProductionArea();
    $('#year_count').val(parseInt(samples_collection.max(function(model) {
      return model.get('year_count');
    }).get('year_count')) + 1);
  });

  /* Event handler for click on the add_parameter button. Adds a new row
   * to the parameter table.
   */
  $("#add_parameter").on("click", function() {
    table.row.add([
      generateParameterCombobox(),
      '<input type="textbox" class="form-control">'
    ]).draw();
    $('html, body').animate({scrollTop: $(document).height() }, 1000);
  });

  /* Event handler for the click on any row in the parameter_table table,
   * selects that row. Used to remove rows.
   */
  $('#parameter_table tbody').on('click', 'tr', function() {
    $(this).toggleClass('selected');
  });

  /* Event handlers for controlling bubbling inside the parameter_table.
   * These handlers are required for the comboboxes and input fields
   * inside the table to work correctly.
   */
  $('#parameter_table tbody').on('click', 'tr input, select', function(event) {
    event.stopPropagation();
  });

  /* Event handler for the blur event on the input forms created inside the
   * parameter_table. The handler, using the values from the table, automatically
   * updates the values of the asp, dsp, psp sum.
   */
  $('#parameter_table tbody').on('blur', 'tr input', function(event) {
    event.stopPropagation();
    recalculateSums();
  });

  $('#parameter_table tbody').on('blur', 'tr select', function(event) {
    event.stopPropagation();
    console.log('update');
    recalculateSums();
  });

  /* Datetime validation */
  $('#sampling_datetime, #reception_datetime, #observation_datetime').on('dp.change', validateDateTime);

  /* Year count validation */
  $('#year_count').on('change', function(event) {
    if (!validateYearCount()) {
      $('#year_count').parent().addClass('has-error');
      validation.set_field_state('count', true);
    } else {
      $('#year_count').parent().removeClass('has-error');
      validation.set_field_state('count', false);
    }
  });

  /* Event handler for the click on the del_parameter button, removes the
   * selected row from the parameter_table.
   */
  $('#del_parameter').on('click', function() {
    table.rows('.selected').remove().draw();
    recalculateSums();
  });

  /* Event handler for form submition */
  /*$('#save').on('click', function () {
    window.global_exit = true;
    doUpload();
  });*/
  $('.savebtn').on('click', function () {
    window.global_exit = true;
    doUpload();
  });

  /*$('#cancel').on('click', function () {
    window.global_exit = true;
    window.location.replace("/dashboard/samples");
  });*/
  $('.cancelbtn').on('click', function () {
    window.global_exit = true;
    window.location.replace("/dashboard/samples");
  });

  /* Event handler for change in the station combobox */
  $('#station').on('change', updateProductionArea);

  $('#observer').on('change', function() {
    if ($('#observer').val() === 'Outro') {
      $('#observer_other').prop('disabled', false);
    } else {
      $('#observer_other').prop('disabled', true);
      $('#observer_other').val('');
    }
  });

  $('#notes').on('change', function() {
    if ($('#notes').val() === 'Outro') {
      $('#notes_other').prop('disabled', false);
    } else {
      $('#notes_other').prop('disabled', true);
      $('#notes_other').val('');
    }
  });
});

function buildDataTable() {
  table = $('#maintable').DataTable({
    pageLength: 25,
    order: [[1, 'desc']],
    language: {
      lengthMenu: 'Mostrar _MENU_ entradas por página',
      zeroRecords: 'Nenhuma entrada encontrada',
      info: 'Página _PAGE_ de _PAGES_',
      infoEmpty: 'Sem entradas',
      infoFiltered: '(de um total de _MAX_ entradas filtradas)',
      paginate: {
        first: 'Primeiro',
        previous: 'Anterior',
        next: 'Próximo',
        last: 'Último'
      },
      search: 'Pesquisar'
    },
    columnDefs: [
      { visible: false, targets: [8, 9, 10, 11, 12, 13] }
    ]
  
  });
}


var table = null;
$(document).ready(function() {
  $('#table-settings h4').on('click', function() {
    $('#table-settings .settings-col').slideToggle(300);
    if ($('#table-settings span').hasClass('glyphicon-chevron-down')) {
      $('#table-settings span').removeClass('glyphicon-chevron-down');
      $('#table-settings span').addClass('glyphicon-chevron-up');
    } else if ($('#table-settings span').hasClass('glyphicon-chevron-up')) {
      $('#table-settings span').removeClass('glyphicon-chevron-up');
      $('#table-settings span').addClass('glyphicon-chevron-down');
    }
  });

  buildDataTable();

  stations_collection.fetch();
  production_areas_collection.fetch();
  sample_parameters_collection.fetch();
  parameters_collection.fetch();
  //samples_collection.fetch();

  /* Debugging the error with the stations_collection fetch */
  stations_collection.on('sync', function () {
    console.log('stations are synced');
    samples_collection.fetch();
  });
  
  //var table = buildDataTable();

  samples_collection.on('add', function(model, collection, options) {
    table.row.add([
      '<a class="btn btn-link btn-xs" href="/dashboard/samples/' + model.get('id') + '" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>',
      model.get('year_count'),
      model.get('year'),
      model.stationName(),
      model.stationProductionArea(),
      new Date(model.get('sampling_datetime')).toISOString().replace('T', ' ').substring(0, 16),
      //moment(new Date(model.get('sampling_datetime'))).format('YYYY-MM-DD HH:mm'),
      //moment(new Date(model.get('reception_datetime'))).format('YYYY-MM-DD HH:mm'),
      new Date(model.get('reception_datetime')).toISOString().replace('T', ' ').substring(0, 16),
      moment(new Date(model.get('observation_datetime'))).format('YYYY-MM-DD'),
      //new Date(model.get('observation_datetime')).toISOString().replace('T', ' ').substring(0, 10),
      model.get('observer'),
      model.get('temperature'),
      model.get('salinity'),
      model.sampleASP(),
      model.sampleDSP(),
      model.samplePSP()
    ]).draw();
  });

  /* Dynamic table column toggling */
  $('#table-settings input').on('change', function(e) {
    e.preventDefault();
    var col = table.column(parseInt($(this).attr('data-column')));
    col.visible( ! col.visible() );
  });
});

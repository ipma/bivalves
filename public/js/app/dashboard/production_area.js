var cols = [{
  name: 'code',
  label: 'Código',
  editable: false,
  cell: 'string'
}, {
  name: 'name',
  label: 'Nome',
  editable: false,
  cell: 'string'
}, {
  name: 'type',
  label: 'Tipo',
  editable: false,
  cell: 'string'
}];

var grid = new Backgrid.Grid({
  columns: cols,
  collection: production_areas_collection
});

$('#main_table').append(grid.render().el);
production_areas_collection.fetch({reset: true});

var express = require('express');
var router  = express.Router();
var models  = require('../../models');


module.exports = {
  name: '/sample_parameter',
  handles: [
  {
    regex: '/',
    method: 'get',
    handle: function(request, response, next) {
      models.SampleParameter.findAll().then(function(results) {
        response.json(results);
      });
    }
  }, {
    regex: '/',
    method: 'post',
    handle: function(request, response, next) {
      models.SampleParameter.create(request.body).then(function(result) {
        response.json(result);
      });
    }
  }, {
    regex: '/:sample_parameter_id',
    method: 'get',
    handle: function(request, response, next) {
      models.SampleParameter.findOne({where: {id: request.params.sample_parameter_id}}).then(function(result) {
        response.json(result);
      });
    }
  }, {
    regex: '/:sample_parameter_id',
    method: 'put',
    handle: function(request, response, next) {
      models.SampleParameter.findOne({where: {id: request.params.sample_parameter_id}}).then(function(result) {
        result.update(request.body).then(function(result) {
          response.json(result);
        });
      });
    }
  }, {
    regex: '/:sample_parameter_id',
    method:'delete',
    handle: function(request, response, next) {
      models.SampleParameter.findOne({where: {id: request.params.sample_parameter_id}}).then(function(result) {
        result.destroy().then(function(result) {
          response.json(result);
        });
      });
    }
  }]
};

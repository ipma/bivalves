var express = require('express');
var router  = express.Router();
var models  = require('../../models');


/* Collection handling:
 * GET   production_area/   returns a list of all elements in the collection
 * POST  production_area/   creates/appends a new element to the collection
 */
/*router.get('/', function(request, response, next) {
  models.ProductionArea.findAll().then(function(results) {
    console.log(results.row);
    response.send('OK');
  });
});

router.post('/', function(request, response, next) {
  response.send('OK');
});

module.exports = router;*/

module.exports = {
  name: '/station',
  handles: [
  {
    regex: '/',
    method: 'get',
    handle: function(request, response, next) {
      models.Station.findAll().then(function(results) {
        response.json(results);
      });
    }
  }, {
    regex: '/',
    method: 'post',
    handle: function(request, response, next) {
      models.Station.create(request.body).then(function(result) {
        response.json(result);
      });
    }
  }, {
    regex: '/:station_id',
    method: 'get',
    handle: function(request, response, next) {
      models.Station.findOne({where: {id: request.params.station_id}}).then(function(result) {
        response.json(result);
      });
    }
  }, {
    regex: '/:station_id',
    method: 'put',
    handle: function(request, response, next) {
      models.Station.findOne({where: {id: request.params.station_id}}).then(function(result) {
        result.update(request.body).then(function(result) {
          response.json(result);
        });
      });
    }
  }, {
    regex: '/:station_id',
    method:'delete',
    handle: function(request, response, next) {
      models.Station.findOne({where: {id: request.params.station_id}}).then(function(result) {
        result.destroy().then(function(result) {
          response.json(result);
        });
      });
    }
  }]
};

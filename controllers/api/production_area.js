var express = require('express');
var router  = express.Router();
var models  = require('../../models');


module.exports = {
  name: '/production_area',
  handles: [
  {
    regex: '/',
    method: 'get',
    handle: function(request, response, next) {
      models.ProductionArea.findAll().then(function(results) {
        response.json(results);
      });
    }
  }, {
    regex: '/',
    method: 'post',
    handle: function(request, response, next) {
      models.ProductionArea.create(request.body).then(function(result) {
        response.json(result);
      });
    }
  }, {
    regex: '/version',
    method: 'get',
    handle: function(request, response, next) {
      models.ProductionAreaVersion.findOne({where: {current: true}}).then(function(result) {
        response.json(result);
      });
    }
  }, {
    regex: '/:id',
    method: 'get',
    handle: function(request, response, next) {
      models.ProductionArea.findOne({where: {code: request.params.id}}).then(function(result) {
        response.json(result);
      });
    }
  }, {
    regex: '/:id',
    method: 'put',
    handle: function(request, response, next) {
      models.ProductionArea.findOne({where: {code: request.params.id}}).then(function(result) {
        result.update(request.body).then(function(result) {
          response.json(result);
        });
      });
    }
  }, {
    regex: '/:id',
    method:'delete',
    handle: function(request, response, next) {
      models.ProductionArea.findOne({where: {code: request.params.id}}).then(function(result) {
        result.destroy().then(function(result) {
          response.json(result);
        });
      });
    }
  }]
};

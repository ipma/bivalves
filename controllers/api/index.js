var fs      = require('fs');
var path    = require('path');
var express = require('express');
var router  = express.Router();


var registerModelEndpoint = function(endpoint_file) {
  var definitions = require('./' + endpoint_file);
  var baseUrl = definitions.name;

  definitions.handles.forEach(function(handle_definition) {
    var modelUrl = baseUrl + handle_definition.regex;

    console.log('Setting up handle for', handle_definition.method.toUpperCase(), 'api' + modelUrl);
    router[handle_definition.method](modelUrl, handle_definition.handle);
  });
};


var endpointsList = [
  'production_area',
  'station',
  'sample',
  'parameter',
  'sample_parameter',
];


endpointsList.forEach(function(element) {
  console.log('Building API endpoints for', element);
  registerModelEndpoint(element);
});

module.exports = router;

var fs      = require('fs');
var express = require('express');
var _       = require('underscore');
var router  = express.Router();
var models  = require('../../models');
var models_utils = require('../../models/utils');
var helpers = require('../helpers');
var async   = require('async');
var ldap    = require('ldapjs');
var csv     = require('express-csv');
var auth    = require('../../auth');
var xls     = require('node-xls');


/* ----------------------------------------------------------------------------
 * Routes
 * ------------------------------------------------------------------------- */

/* GET dashboard login page. */
router.get('/login', function(req, res, next) {
  res.render('dashboard/login', {
    title: 'Login',
    active: '/dashboard',
  });
});


/* POST dashboard login page */
router.post('/login', auth.authenticate, function(req, res, next) {
  res.redirect('/dashboard/samples');
});


/* GET dashboard logout and redirect to index */
router.get('/logout', function(req, res, next) {
  if (req.hasOwnProperty('session')) {
    req.session.destroy(function(err) {
      res.redirect('/dashboard');
    });
  } else {
    res.redirect('/dashboard');
  }
});


/* GET dashboard home page. */
router.get('/', function(req, res, next) {
  models.ProductionArea.findAll().then(function(results) {
    res.render('dashboard/base', helpers.buildContext({
      title: 'Área principal',
      active: '/dashboard',
      login: req.session.hasOwnProperty('user'),
      user: req.session.user || null
    }, {
      production_areas: JSON.stringify(results)
    }));
  });
});


/* GET dashboard production areas page */
router.get('/production_areas', function(req, res, next) {
  /*models.ProductionArea.findAll().then(function(results) {
    res.render('dashboard/production_area', helpers.buildContext({
      title: 'Zonas de produção',
      active: '/dashboard/production_areas',
      login: req.session.hasOwnProperty('user')
    }, {
      production_areas: JSON.stringify(results)
    }));
  });*/
  models_utils.get_current_production_areas().then(function(results) {
    console.log(results);
    res.render('dashboard/production_area', helpers.buildContext({
      title: 'Zonas de produção',
      active: '/dashboard/production_areas',
      login: req.session.hasOwnProperty('user')
    }, {
      production_areas: JSON.stringify(results).replace("\'", "\\\'")
    }));
  });
});

/* GET dashboard stations page */
router.get('/stations', function(req, res, next) {
  models.Station.findAll().then(function(results) {
    res.render('dashboard/station', helpers.buildContext({
      title: 'Estações de amostragem',
      active: '/dashboard/stations',
      login: req.session.hasOwnProperty('user')
    }, {
      stations: JSON.stringify(results)
    }));
  });
});

/* GET dashboard station edit page */
router.get('/stations/:id', function(req, res, next) {
  models.Station.findById(req.params.id).then(function(station) {
    models.ProductionArea.findAll().then(function(production_areas) {
      res.render('dashboard/station_edit', helpers.buildContext({
        title: 'Editar estação',
        active: '/dashboard/stations',
        login: req.session.hasOwnProperty('user')
      }, {
        station: JSON.stringify(station),
        production_areas: production_areas
      }));
    });
  });
});

/* POST dashboard station edit */
router.post('/stations/:id', function(req, res, next) {
  models.Station.findById(req.params.id).then(function(station) {
    station.update(request.body).then(function(result) {
      response.json(result);
    });
  });
});


/* GET dashboard samples page */
router.get('/samples', function(req, res, next) {
  models.Sample.findAll().then(function(results) {
    res.render('dashboard/sample', helpers.buildContext({
      title: 'Amostras',
      active: '/dashboard/samples',
      login: req.session.hasOwnProperty('user')
    }, {
      samples: JSON.stringify(results)
    }));
  });
});

/* GET dashboard new sample page */
router.get('/samples/new', function(req, res, next) {
  models.Station.findAll().then(function(results) {
    res.render('dashboard/sample_new', helpers.buildContext({
      title: 'Nova amostra',
      active: '/dashboard/samples',
      login: req.session.hasOwnProperty('user')
    }, {
      stations: results
    }));
  });
});

/* POST dashboard new sample */
router.post('/samples/new', function(req, res, next) {
  var sample = {
    year: new Date().getFullYear(),
    year_count: req.body.year_count,
    sampling_datetime: req.body.sampling_datetime,
    reception_datetime: req.body.reception_datetime,
    observation_datetime: req.body.observation_datetime,
    temperature: req.body.temperature,
    salinity: req.body.salinity,
    observer: req.body.observer,
    notes: req.body.notes,
    station_id: parseInt(req.body.station)
  };

  models.Sample.findAll({
    order: 'year_count DESC',
    limit: 1,
    where: {
      year: new Date().getFullYear()
    },
  }).then(function(result) {
    /*if (result.length > 0) {
      yc = result[0].getDataValue('year_count');
      sample.year_count = ++yc;
    } else {
      sample.year_count = 1;
    }*/

    models.Sample.create(sample).then(function(result) {
      /* After creating the sample we create the parameter_sample entries */
      var data = [];
      if (req.body.parameters) {
        req.body.parameters.forEach(function(elm, index) {
          data.push({
            value: elm.value,
            parameter_id: parseInt(elm.parameter),
            sample_id: result.id
          });
        });
      }

      /* Bulk create of sample_parameter entries */
      models.SampleParameter.bulkCreate(data).then(function() {
        res.json({
          status: 200,
          redirect: '/dashboard/samples'
        });
      });
    });
  });
});


/* GET method for CSV export */
router.get('/samples/export', function(req, res, next) {
  var current_year = new Date().getFullYear();
  var all_observations = [];

  models.Sample.findAll({
    where: {
      year: current_year
    }
  }).then(function(samples) {
    async.each(samples, function(sample, callback) {
      var new_entry = {
        'Nº de amostra': sample.year_count,
        'Ano': sample.year,
        'Data de colheita': new Date(sample.sampling_datetime).toISOString().substring(0, 10),
        'Hora de colheita': new Date(sample.sampling_datetime).toISOString().substring(11, 16),
        'Data de recepção': new Date(sample.reception_datetime).toISOString().substring(0, 10),
        'Hora de recepção': new Date(sample.reception_datetime).toISOString().substring(11, 16),
        'Data de observação': new Date(sample.observation_datetime).toISOString().substr(0, 10),
        'Temperatura': sample.temperature,
        'Salinidade': sample.salinity,
        'Observador': sample.observer,
        'Notas': sample.notes
      };
      sample.getStation().then(function(station) {
        new_entry['Local de amostragem'] = station.name;
        new_entry['Latitude'] = station.lat;
        new_entry['Longitude'] = station.lon;
        models.ProductionArea.findOne({
          where: {
            code: station.production_area_code
          }
        }).then(function(production_area) {
          new_entry['Código'] = production_area.code;
          new_entry['Zona de Produção'] = production_area.name;
          /* Prepare raw query text */
          var raw = 'SELECT p.name, COALESCE(sp.value, 0) FROM (SELECT sample_id, parameter_id, value FROM sample_parameter ' + 
                    'WHERE sample_id=' + sample.id + ') AS sp RIGHT OUTER JOIN parameter AS p ON sp.parameter_id=p.id;';
          /* Send the raw query and pivot the results */
          models.sequelize.query(raw, {
            type: models.sequelize.QueryTypes.SELECT
          }).then(function(sp) {
            for (var i = 0; i < sp.length; i++) {
              new_entry[sp[i].name] = sp[i].coalesce;
            }
            all_observations.push(new_entry);
            callback();
          });
        });
      });
    }, function() {
      /* Build the header using the first object in the array */
      if (all_observations.length > 0) {
        var first = all_observations[0];
        all_observations.unshift(Object.keys(first));
      }
      /*var tool = new xls();
      var file = tool.json2xls(all_observations);
      fs.writeFile('/home/vagrant/observations.xlsx', file, 'binary', function () {
        res.download('/home/vagrant/observations.xlsx', 'observacoes.xlsx');
      });*/
      console.log(res);
      res.csv(all_observations);
    });
  });
});


/* GET dashboard new sample page */
router.get('/samples/:id', function(req, res, next) {
  models.Station.findAll().then(function(results) {
    res.render('dashboard/sample_edit', helpers.buildContext({
      title: 'Editar amostra',
      active: '/dashboard/samples',
      login: req.session.hasOwnProperty('user')
    }, {
      stations: results,
      sample_id: req.params.id
    }));
  });
});


/* POST dashboard update sample */
router.post('/samples/:id', function(req, res, next) {
  models.Sample.findById(req.params.id).then(function(result) {
    result.sampling_datetime = req.body.sampling_datetime;
    result.reception_datetime = req.body.reception_datetime;
    result.observation_datetime = req.body.observation_datetime;
    result.temperature = req.body.temperature;
    result.salinity = req.body.salinity;
    result.sample_volume = req.body.sample_volume;
    result.observer = req.body.observer;
    result.notes = req.body.notes;
    result.station_id = parseInt(req.body.station);
    result.year_count = parseInt(req.body.year_count);

    result.save().then(function() {
      models.SampleParameter.findAll({
        where: {
          sample_id: req.params.id
        }
      }).then(function(results) {
        var params_list = results.map(function(current, index, array) {
          return current.parameter_id;
        });

        var new_data = [];
        var update_data = [];
        var delete_data = [];
        if (req.body.parameters) {
          req.body.parameters.forEach(function(elm, index) {
            var new_id = parseInt(elm.parameter);
            if (params_list.indexOf(new_id) !== -1) {
              update_data.push({
                value: elm.value,
                parameter_id: parseInt(elm.parameter),
                sample_id: result.id
              });
            } else {
              new_data.push({
                value: elm.value,
                parameter_id: parseInt(elm.parameter),
                sample_id: result.id
              });
            }
          });
        }

        var form_list = req.body.parameters.map(function(current, index, array) {
          return current.parameter;
        });
        for (var i = 0; i < params_list.length; i++) {
          if (form_list.indexOf(params_list[i]) === -1) {
            delete_data.push(params_list[i]);
          }
        }

        /* Update or create the sample parameters stuff. At the
         * end, send the response with the redirect URL
         */
        models.SampleParameter.bulkCreate(new_data).then(function() {
          models.SampleParameter.destroy({
            where: {
              parameter_id: delete_data
            }
          }).then(function() {
            async.each(update_data, function(sp, callback) {
              models.SampleParameter.update({
                value: sp.value
              }, {
                where: {
                  sample_id: sp.sample_id,
                  parameter_id: sp.parameter_id
                }
              }).then(function() {
                callback();
              });
            }, function() {
              res.json({
                status: 200,
                redirect: '/dashboard/samples'
              });
            });
          });
        });

      });
    });
  });

});

router.get('/samples/:id/remove', function (req, res, next) {
  var sample_id = req.params.id;
  models.SampleParameter.destroy({
    where: {
      sample_id: req.params.id
    }
  }).then(function(result) {
    models.Sample.destroy({
      where: {
        id: req.params.id
      }
    }).then(function() {
      res.redirect('/dashboard/samples');
    });
  });
});


module.exports = router;

var express = require('express');
var _       = require('underscore');


module.exports = {
  buildContext: function (context, extension) {
    var template_context = {
      title: 'Bivalves'
    };

    return _.extend(template_context, context, extension);
  }
}

var ldap = require('ldapjs');
var _    = require('underscore');


/* ----------------------------------------------------------------------------
 * Authentication, authorization and session management.
 *
 * Authetication is provided using LDAP.
 * ------------------------------------------------------------------------- */

/* LDAP auth client */
var authclient = ldap.createClient({
  url: 'ldap://192.168.150.28:389'
});

/* Auth functions */
function restrict(req, res, next) {
  if (req.session.user || _.contains(['/login', '/dashboard/login'], req.path)) {
    next();
  } else {
    res.redirect('/dashboard/login');
  }
}


function authenticate(req, res, next) {
  var dn = 'uid=' + req.body['input-username'] + ',ou=ipma,dc=intranet,dc=meteo,dc=pt';
  var secret = req.body['input-password'];
  authclient.bind(dn, secret, function(err) {
    var sess = req.session;
    if(err) {
      if (err.code === 49) {
        res.render('dashboard/login', {
          title: 'Login',
          active: '/dashboard',
          error: 'Username ou password incorrectos'
        });
      } else {
        res.render('dashboard/login', {
          title: 'Login',
          active: '/dashboard',
          error: 'Ocorreu um erro desconhecido'
        });
      }
    } else {
      sess.user = req.body['input-username'];
      next();
    }
  });
}

module.exports = {
  authclient: authclient,
  restrict: restrict,
  authenticate: authenticate
};
